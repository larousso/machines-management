var path = require('path');
var elmSource = __dirname + '/src';

console.log('Elm source', elmSource);

module.exports = {
    debug: true,
    devtool: 'source-map',
    entry: {
        alerts: './src/alerts/alerts.js',
        correlations: './src/correlations/correlations.js'
    },
    output: {
        path: './dist/',
        filename: '[name].js',
    },
    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.elm']
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel', // 'babel-loader' is also a legal name to reference
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                loader: 'elm-webpack'
            }
        ],
        noParse: /\.elm$/

    }
};