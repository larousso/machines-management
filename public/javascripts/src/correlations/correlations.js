import Elm from "./Correlations.elm";

const app = Elm.Main.embed(document.getElementById('main'));

app.ports.location.send(window.location.host);
