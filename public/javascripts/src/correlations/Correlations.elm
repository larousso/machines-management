port module Main exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Json.Decode exposing (..)
import WebSocket
import Date


-- Utils


webSocketEndpoint : String -> String
webSocketEndpoint str =
    "ws://" ++ str ++ "/api/correlations/ws"


stringToFloat : Decoder Float
stringToFloat =
    string
        `andThen`
            \val ->
                case Date.fromString val of
                    Err err ->
                        fail err

                    Ok ms ->
                        Date.toTime ms |> succeed


value : Decoder Value
value =
    object2 Value
        ("timestamp" := stringToFloat)
        ("value" := Json.Decode.float)


correlation : Decoder Correlation
correlation =
    object8 Correlation
        ("machineId" := Json.Decode.string)
        ("values" := Json.Decode.list value)
        ("temperatures" := Json.Decode.list value)
        ("pressures" := Json.Decode.list value)
        ("humidity" := Json.Decode.list value)
        ("humidityCorr" := Json.Decode.float)
        ("pressureCorr" := Json.Decode.float)
        ("temperatureCorr" := Json.Decode.float)


correlations : Decoder (List Correlation)
correlations =
    Json.Decode.list correlation



-- Model


type alias Value =
    { timestamp : Float, value : Float }


type alias Correlation =
    { machineId : String, values : List Value, temperatures : List Value, pressures : List Value, humidity : List Value, humidityCorr : Float, pressureCorr : Float, temperatureCorr : Float }


type alias Model =
    { correlations : List Correlation, server : Maybe String }


type Msg
    = NewMessage String
    | Server String



-- Init


init : ( Model, Cmd Msg )
init =
    ( Model [] Nothing, Cmd.none )



-- View
--displayValues: Correlation -> List (Html Msg)
--displayValues correlation =
--    List.map (\value -> span [class "label label-default"] [toString value |> text]) alert.lastValues


buildId : Correlation -> String
buildId correlation =
    "graphic-" ++ correlation.machineId


displayCorrelation : Correlation -> Html Msg
displayCorrelation correlation =
    div [ class "col-md-3" ]
        [ div [ class "panel panel-default" ]
            [ div [ class "panel-body" ]
                [ h4 [] [ text "Correlation " ]
                , div []
                    [ "Machine id " ++ correlation.machineId |> text
                    ]
                , p []
                    [ text "Humidity : "
                    , span [ class "badge" ] [ toString correlation.humidityCorr |> text ]
                    , br [] []
                    , text "temperature : "
                    , span [ class "badge" ] [ toString correlation.temperatureCorr |> text ]
                    , br [] []
                    , text "Humidity : "
                    , span [ class "badge" ] [ toString correlation.humidityCorr |> text ]
                    , br [] []
                    ]
                ]
            ]
        ]


view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ div [ class "row" ]
            [ div [ class "col-md-12" ]
                [ h1 [] [ text "Correlations" ]
                ]
            ]
        , div [ class "row" ]
            [ div [] (List.reverse model.correlations |> List.sortBy .machineId |> List.map displayCorrelation)
            ]
        ]



-- Update


buildChartMsg : Correlation -> { id : String, data : Correlation }
buildChartMsg correlation =
    { id = (buildId correlation)
    , data = correlation
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Server str ->
            --Debug.log ("Server : " ++ str)
            ( { model | server = Just str }, Cmd.none )

        NewMessage str ->
            case decodeString correlation str of
                Ok val ->
                    --Debug.log ("New val : " ++ toString val)
                    ( { model | correlations = val :: (List.filter (\v -> v.machineId /= val.machineId) model.correlations) }, buildChartMsg val |> chart )

                Err message ->
                    Debug.crash ("Error parsing json : " ++ message)
                        ( model, Cmd.none )



-- Subscriptions


port chart : { id : String, data : Correlation } -> Cmd msg


port location : (String -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.server of
        Just str ->
            WebSocket.listen (webSocketEndpoint str) NewMessage

        Nothing ->
            location Server



-- Main


main : Program Never
main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
