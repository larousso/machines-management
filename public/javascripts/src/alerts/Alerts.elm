port module Main exposing (..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode exposing (..)
import Json.Encode as Json
import WebSocket

-- Utils

server : String -> String
server str =
  "ws://" ++ str ++ "/api/alerts/ws"


alert: Decoder Alert
alert =
    object5 Alert
        ("id" := Json.Decode.string)
        ("lastValues" := Json.Decode.list Json.Decode.float)
        ("mean" := Json.Decode.float)
        ("current" := Json.Decode.float)
        ("current_alert" := Json.Decode.float)

-- Model

type alias Alert = {id: String, lastValues: List Float, mean: Float, current: Float, current_alert: Float}

type alias Model = { alerts: List Alert, server: Maybe String}

type Msg = NewMessage String | Server String

-- Init

init: (Model, Cmd Msg)
init =
    (Model [] Nothing, Cmd.none)

-- View

displayValues: Alert -> List (Html Msg)
displayValues alert =
    List.map (\value -> span [class "label label-default"] [toString value |> text]) alert.lastValues

displayAlert: Alert -> Html Msg
displayAlert alert =
    div [class "panel panel-default"] [
        div [class "panel-body"] [
            h1 [] [span [class "glyphicon glyphicon-alert text-danger"] [], text "new alert"],
            div [] [
                "Machine id " ++ alert.id |> text,
                br [] [],
                text "Alert at ",
                span [class "label label-primary"] [toString alert.current_alert |> text],
                br [] [],
                text "Current value ",
                span [class "label label-danger"] [toString alert.current |> text]
            ],
            div [] (displayValues alert |> List.append [text "Last Values : "] ),
            div [id alert.id] []
        ]
    ]


view: Model -> Html Msg
view model =
    div [class "container"] [
        div [class "row"] [
            div [class "col-md-12"] [
                h1 [] [ text "Alerts"]
            ]
        ],
        div [class "row"] [
            div [] (List.map displayAlert model.alerts)
        ]
    ]

-- Update

update: Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Server str ->
            Debug.log ("Server : " ++ str)
            ({model | server = Just str}, Cmd.none)
        NewMessage str ->
            case decodeString alert str of
                Ok val ->
                    Debug.log ("New val : " ++ toString val)
                    ({model | alerts = val :: model.alerts}, Cmd.none)
                Err message ->
                    Debug.crash ("Error parsing json : " ++ message)
                    (model, Cmd.none)


-- Subscriptions

port location: (String -> msg) -> Sub msg

--port chart : { id : String, data : List Float } -> Cmd msg


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.server of
        Just str ->
             WebSocket.listen (server str) NewMessage
        Nothing ->
            location Server

-- Main

main = App.program
           { init = init
           , view = view
           , update = update
           , subscriptions = subscriptions
           }