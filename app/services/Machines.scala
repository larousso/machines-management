package services

import java.time.LocalDateTime

import akka.NotUsed
import akka.actor.{ActorSystem, Cancellable}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import play.api.libs.json.Json
import services.Machines.Machine

import scala.concurrent.Future
import scala.concurrent.duration.{DurationDouble, FiniteDuration}

/**
  * Created by adelegue on 22/04/2016.
  */
case class Machines(apiRoot: String)(implicit actorSystem: ActorSystem) {

  implicit val ec = actorSystem.dispatcher
  implicit val materializer = ActorMaterializer()

  val http = Http(actorSystem)

  private def id(uri: String): String = {
    val regex = ".*/machine/(.*)".r
    uri match {
      case regex(id) => id
      case _ => uri
    }
  }

  def getAllMachines: Future[Seq[String]] = {

    http.singleRequest(HttpRequest(uri = s"$apiRoot/machines"))
      .flatMap {
        case HttpResponse(StatusCodes.OK, headers, entity, _) =>
          entity.dataBytes
            .map(_.utf8String)
            .runFold("")(_ ++ _)
            .map {
              Json.parse
            }
            .map {
              _.as[Seq[String]]
            }
        case HttpResponse(_, headers, entity, _) =>
          Future.failed(new RuntimeException("Getting machines failed"))
      }
  }

  def getMachine(templatedUri: String): Future[Option[(String, Machine)]] = {
    http.singleRequest(HttpRequest(uri = templatedUri.replace("$API_ROOT", apiRoot)))
      .flatMap {
        case HttpResponse(StatusCodes.OK, headers, entity, _) =>
          entity.dataBytes.map(_.utf8String).runFold("")(_ + _)
            .map {
              Json.parse
            }
            .map {
              Machines.format.reads
            }
            .map {
              _.get
            }
            .map { m => Some((id(templatedUri), m)) }
        case HttpResponse(StatusCodes.NotFound, headers, entity, _) =>
          Future.successful(None)
        case HttpResponse(_, headers, entity, _) =>
          Future.failed(new RuntimeException(s"Getting machine $templatedUri failed"))
      }
  }

}


object Machines {

  case class Machine(name: String, timestamp: LocalDateTime, current: Double, state: String, location: String, current_alert: Double, `type`: String)

  implicit val format = Json.format[Machine]

}