package services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{Flow, Source}
import akka.stream.stage._
import model.Alert
import services.Machines._

import scala.concurrent.duration._

/**
  * Created by adelegue on 24/04/2016.
  */
case class MachinesMonitor(machines: Machines)(implicit val actorSystem: ActorSystem) {

  implicit val ec = actorSystem.dispatcher

  implicit class FlowDuplicator[In, Out, Mat](s: Flow[In, Out, Mat]) {
    def aggregate(n: Int): Flow[In, Seq[Out], Mat] = s.via(new Aggregator(n))
  }

  val windowSize = 5 //5 * 5 * 60

  val alerts = Flow[(String, Machine)].aggregate(windowSize)
    .map { lst =>
      val (id, machine) = lst.last
      val values = lst.map {
        case (_, m) => m.current
      }
      val mean = values.sum / values.size
      Alert(id, values, mean, machine.current, machine.current_alert)
    }

  def alertsStream(delay: FiniteDuration = 0.second, interval: FiniteDuration = 20.milliseconds): Source[Alert, NotUsed] =
    Source.fromFuture(machines.getAllMachines)
      .flatMapConcat { m =>
        Source.tick(delay, interval, "next")
          .mapConcat(_ => m.toList)
          .mapAsync(4)(machines.getMachine)
          .mapConcat(_.toList)
          .groupBy(m.size, _._1)
          .async
          .via(alerts)
          .filter(a => a.current > a.current_alert)
          .mergeSubstreams
      }

}

class Aggregator[A](n: Int) extends GraphStage[FlowShape[A, Seq[A]]] {

  val in = Inlet[A]("Aggregator.in")
  val out = Outlet[Seq[A]]("Aggregator.out")

  val shape = FlowShape.of(in, out)

  private var buf = Vector.empty[A]

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic =
    new GraphStageLogic(shape) {

      setHandler(in, new InHandler {
        override def onPush(): Unit = {
          val elem = grab(in)
          buf :+= elem
          if (buf.size == n + 1) {
            buf = buf.drop(1)
          }
          emit(out, buf)
        }
      })

      setHandler(out, new OutHandler {
        override def onPull(): Unit = {
          pull(in)
        }
      })
    }
}