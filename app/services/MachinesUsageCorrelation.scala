package services

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import model.Correlation
import model.Record

import scala.concurrent.duration.{DurationLong, FiniteDuration}

/**
  * Created by adelegue on 27/08/2016.
  */
case class MachinesUsageCorrelation(machines: Machines, sensors: Sensors)(implicit val actorSystem: ActorSystem) {

  def correlationStream(delay: FiniteDuration = 0.second, interval: FiniteDuration = 20.milliseconds): Source[Correlation, NotUsed] =
    Source.fromFuture(machines.getAllMachines)
      .flatMapConcat { m =>
        Source.tick(delay, interval, "next")
          .mapAsync(1)(_ => sensors.getSensor)
          .mapConcat(_.toList)
          .flatMapMerge(4, s => {
            Source(m.toList)
              .mapAsync(1)(machines.getMachine)
              .mapConcat(_.toList)
              .map { mach =>
                (mach._1, s, mach._2)
              }
          })
          .groupBy(m.size, _._1)
          .async
          .scan(Correlation()) { (acc, t) =>
            val (id, sensor, machine) = t
            val lastValues = Record(machine.timestamp, machine.current) :: acc.values
            val temperatures = Record(sensor.temperature.timestamp, sensor.temperature.value) :: acc.temperatures
            val pressures = Record(sensor.pressure.timestamp, sensor.pressure.value) :: acc.pressures
            val humidity = Record(sensor.humidity.timestamp, sensor.humidity.value) :: acc.humidity
            Correlation(Some(id), lastValues, temperatures, pressures, humidity)
          }
          .filter(_.machineId.isDefined)
          .mergeSubstreams
      }

}
