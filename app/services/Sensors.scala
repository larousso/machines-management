package services

import java.time.LocalDateTime

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.stream.ActorMaterializer
import play.api.libs.json._
import services.Sensors._

import scala.concurrent.Future

/**
  * Created by adelegue on 27/08/2016.
  */
case class Sensors(apiRoot: String)(implicit actorSystem: ActorSystem) {

  implicit val ec = actorSystem.dispatcher
  implicit val materializer = ActorMaterializer()

  val uri = s"$apiRoot/env-sensor"
  val http = Http(actorSystem)

  def getSensor: Future[Option[Sensor]] = {
    http.singleRequest(HttpRequest(uri = uri))
      .flatMap {
        case HttpResponse(StatusCodes.OK, headers, entity, _) =>
          entity.dataBytes
            .map(_.utf8String)
            .runFold("")(_ ++ _)
            .map {
              Json.parse
            }
            .map {
              Sensors.format.reads
            }
            .map {
              _.get
            }
            .map {
              Some.apply
            }
        case HttpResponse(StatusCodes.NotFound, headers, entity, _) =>
          Future.successful(None)
        case HttpResponse(_, headers, entity, _) =>
          entity.dataBytes
            .map(_.utf8String)
            .runFold("")(_ ++ _)
            .flatMap { msg =>
              Future.failed(new RuntimeException(s"Getting sensor failed : \n$msg"))
            }
      }
  }

}


object Sensors {

  case class Sensor(pressure: Value, temperature: Value, humidity: Value)

  case class Value(timestamp: LocalDateTime, value: Double)

  val valueRead = Reads[Value] {
    case JsArray(seq) =>
      val v1 :: v2 :: Nil = seq.toList
      Reads.localDateTimeReads("yyyy-MM-dd'T'HH:mm:ss").reads(v1).flatMap { d =>
        Reads.DoubleReads.reads(v2).map { value =>
          Value(d, value)
        }
      }
    case e =>
      println(s"Error : $e")
      JsError("Wrong format for Value")
  }

  val valueWrite = Writes[Value] { v =>
    JsArray(Seq(
      Writes.DefaultLocalDateTimeWrites.writes(v.timestamp),
      Writes.DoubleWrites.writes(v.value)
    ))
  }

  implicit val valueFormat = Format(valueRead, valueWrite)

  implicit val format = Json.format[Sensor]
}