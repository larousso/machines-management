package controllers

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import play.api.libs.json.Json
import play.api.mvc._
import services.Machines._

import scala.util.Random

/**
  * Created by adelegue on 28/04/2016.
  */
class Mock extends Controller {

  var counter = 0
  val random = new Random()

  def machines() = Action {
    //val machines = Seq("$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5")
    val machines = Seq("$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5", "$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c6")

    Ok(Json.toJson(machines))
  }

  def machine(id: String) = Action {
    val machine = counter match {
      case c if c % 100 == 0 =>
        val value = random.nextInt(20)
        Machine("DMG DMU 40eVo [#50]", LocalDateTime.now(), value + 14, "working", "0.0,0.0", 14, "mill")
      case _ =>
        val value = random.nextInt(14)
        Machine("DMG DMU 40eVo [#50]", LocalDateTime.now(), value, "working", "0.0,0.0", 14, "mill")
    }
    counter += 1
    Ok(Json.toJson(machine))
  }

  def sensor() = Action {
    val date = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").format(LocalDateTime.now())
    Ok(
      s"""
          {
            "pressure":["$date",${random.nextInt(1000)}],
            "temperature":["$date",${random.nextInt(30)}],
            "humidity":["$date",${random.nextInt(100)}]
          }
       """)
  }
}
