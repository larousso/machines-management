package controllers

import actor._
import akka.Done
import akka.actor.{ActorRef, ActorSystem}
import akka.stream.scaladsl.Sink
import akka.stream.{ActorAttributes, ActorMaterializer, Supervision}
import akka.util.Timeout
import play.api.Logger
import play.api.libs.json._
import play.api.libs.streams.ActorFlow
import play.api.mvc._
import model.Alert
import model.Correlation
import services._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt

/**
  * Created by adelegue on 22/04/2016.
  */
case class Application(machinesMonitor: MachinesMonitor, machinesUsageCorrelation: MachinesUsageCorrelation, alertsActor: ActorRef, correlationsActor: ActorRef, actorSystem: ActorSystem) extends Controller {

  implicit val system = actorSystem
  implicit val materializer = ActorMaterializer()

  machinesMonitor
    .alertsStream()
    .addAttributes(ActorAttributes.supervisionStrategy {
      case e =>
        Logger.logger.error("Error ", e)
        Supervision.resume
    })
    .runWith {
      Sink.actorRef(alertsActor, Done)
    }

  machinesUsageCorrelation
    .correlationStream(interval = 30.second)
    .addAttributes(ActorAttributes.supervisionStrategy {
      case e =>
        Logger.logger.error("Error ", e)
        Supervision.resume
    })
    .runWith {
      Sink.actorRef(correlationsActor, Done)
    }


  def index() = Action { implicit request =>
    Ok(views.html.index())
  }

  def alertsHtml() = Action { implicit request =>
    Ok(views.html.alerts())
  }

  def correlationsHtml() = Action { implicit request =>
    Ok(views.html.correlation())
  }

  def alerts() = Action.async {
    import model.Alert._
    import akka.pattern._
    implicit val timeout = Timeout(1.second)

    (alertsActor ? AlertsActor.GetAlerts)
      .mapTo[List[Alert]]
      .map { l => Json.toJson(l) }
      .map { r => Ok(r) }
  }

  def webSocketAlerts = WebSocket.accept[JsValue, JsValue] { r =>
    ActorFlow.actorRef(out => WebSocketAlertsActor.props(out, alertsActor))
  }

  def correlations() = Action.async {
    import model.Correlation._
    import akka.pattern._
    implicit val timeout = Timeout(1.second)

    (correlationsActor ? CorrelationsActor.GetCorrelations)
      .mapTo[Map[String, Correlation]]
      .map { l => Json.toJson(l) }
      .map { r => Ok(r) }
  }


  def webSocketCorrelations = WebSocket.accept[JsValue, JsValue] { r =>
    ActorFlow.actorRef(out => WebSocketCorrelationsActor.props(out, correlationsActor))
  }

}
