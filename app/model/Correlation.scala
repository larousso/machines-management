package model

import java.time.LocalDateTime

import play.api.libs.json.Json
import utils.FList

/**
  * Created by adelegue on 05/09/2016.
  */

case class Correlation(machineId: Option[String] = None, values: FList[Record] = FList.empty[Record](Correlation.listSize), temperatures: FList[Record] = FList.empty[Record](Correlation.listSize), pressures: FList[Record] = FList.empty[Record](Correlation.listSize), humidity: FList[Record] = FList.empty[Record](Correlation.listSize))

case class Record(timestamp: LocalDateTime, value: Double)

object Correlation {
  val listSize = 5000
  implicit val formatRecord = Json.format[Record]
  implicit val format = Json.format[Correlation]

}
