package model

import play.api.libs.json.Json

/**
  * Created by adelegue on 05/09/2016.
  */
case class Alert(id: String, lastValues: Seq[Double], mean: Double, current: Double, current_alert: Double)

object Alert {
  implicit val format = Json.format[Alert]
}