package actor

import akka.actor.{Actor, ActorRef, Props}
import model.Correlation

/**
  * Created by adelegue on 29/08/2016.
  */
class CorrelationsActor extends Actor {

  import CorrelationsActor._

  var correlations: Map[String, Correlation] = Map.empty[String, Correlation]

  var toNotify = List.empty[ActorRef]

  override def receive: Receive = {
    case c: Correlation if c.machineId.isDefined =>
      correlations = correlations + (c.machineId.get -> c)
      toNotify.foreach { listener =>
        listener ! c
      }
    case Listener(actorRef) =>
      toNotify ::= actorRef
    case ForgiveMe(ref) =>
      toNotify = toNotify.filterNot(_ == ref)
    case GetCorrelations => sender() ! correlations
  }
}

object CorrelationsActor {

  def props() = Props[CorrelationsActor]

  case object GetCorrelations

  case class Listener(actorRef: ActorRef)

  case class ForgiveMe(actorRef: ActorRef)

}
