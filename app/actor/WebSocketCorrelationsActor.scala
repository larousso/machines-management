package actor

import akka.actor.{Actor, ActorRef, Props}
import play.api.libs.json.{JsObject, JsValue, Json}
import utils.{FList, Stats}
import model.Correlation
import model.Record

/**
  * Created by adelegue on 29/08/2016.
  */
object WebSocketCorrelationsActor {
  def props(out: ActorRef, correlationsActor: ActorRef): Props = Props(classOf[WebSocketCorrelationsActor], out, correlationsActor)
}

class WebSocketCorrelationsActor(out: ActorRef, correlationsActor: ActorRef) extends Actor {

  override def preStart(): Unit = {
    correlationsActor ! CorrelationsActor.Listener(self)
  }

  override def postStop(): Unit = {
    correlationsActor ! CorrelationsActor.ForgiveMe(self)
  }

  override def receive: Receive = {
    case any: JsValue =>
      println(s"new Message $any")
    case a: Correlation =>
      import model.Correlation._

      out ! (Json.toJson(a).as[JsObject] ++ Json.obj(
        "humidityCorr" -> correlation(a.values, a.humidity),
        "pressureCorr" -> correlation(a.values, a.pressures),
        "temperatureCorr" -> correlation(a.values, a.temperatures)
      ))
  }

  def correlation(l1: FList[Record], l2: FList[Record]): Double = {
    Stats.correlationCoef(l1.toList.map(_.value), l2.toList.map(_.value)).getOrElse(0d)
  }

}
