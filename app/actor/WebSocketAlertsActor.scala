package actor

import model.Alert
import akka.actor.{Actor, ActorRef, Props}
import play.api.libs.json.{JsValue, Json}

/**
  * Created by adelegue on 26/08/2016.
  */

object WebSocketAlertsActor {
  def props(out: ActorRef, alertsActor: ActorRef): Props = Props(classOf[WebSocketAlertsActor], out, alertsActor)
}

class WebSocketAlertsActor(out: ActorRef, alertsActor: ActorRef) extends Actor {

  override def preStart(): Unit = {
    alertsActor ! AlertsActor.Listener(self)
  }

  override def postStop(): Unit = {
    alertsActor ! AlertsActor.ForgiveMe(self)
  }

  override def receive: Receive = {
    case any: JsValue =>
      println(s"new Message $any")
    case a: Alert =>
      import model.Alert._
      out ! Json.toJson(a)
  }
}
