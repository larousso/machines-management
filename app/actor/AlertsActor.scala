package actor

import akka.actor.{Actor, ActorRef, Props}
import model.Alert

/**
  * Created by adelegue on 23/04/2016.
  */
class AlertsActor extends Actor {

  import AlertsActor._

  var lastAlerts = List.empty[Alert]

  var toNotify = List.empty[ActorRef]

  override def receive: Receive = {
    case alert: Alert =>
      if (lastAlerts.size == 5) {
        lastAlerts = alert :: lastAlerts.take(4)
      } else {
        lastAlerts ::= alert
      }
      toNotify.foreach { listener =>
        listener ! alert
      }
    case Listener(actorRef) =>
      toNotify ::= actorRef
    case ForgiveMe(ref) =>
      toNotify = toNotify.filterNot(_ == ref)
    case GetAlerts => sender() ! lastAlerts
  }
}

object AlertsActor {

  def props() = Props[AlertsActor]

  case object GetAlerts

  case class Listener(actorRef: ActorRef)

  case class ForgiveMe(actorRef: ActorRef)

}
