package utils

/**
  * Created by adelegue on 03/09/2016.
  */
object Stats {

  def correlationCoef(list1: List[Double], list2: List[Double]): Option[Double] = {
    if (list1.size != list2.size) {
      None
    }
    val n = list1.size

    val sum1 = list1.sum
    val sum2 = list2.sum

    val sum1Sq = list1.foldLeft(0.0)(_ + Math.pow(_, 2))
    val sum2Sq = list2.foldLeft(0.0)(_ + Math.pow(_, 2))

    val pSum = (for ((e1, e2) <- list1 zip list2) yield e1 * e2).sum

    val numerator = pSum - (sum1 * sum2 / n)
    val denominator = Math.sqrt((sum1Sq - Math.pow(sum1, 2) / n) * (sum2Sq - Math.pow(sum2, 2) / n))
    if (denominator == 0) None else Option(numerator / denominator)
  }

}
