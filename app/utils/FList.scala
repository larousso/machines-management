package utils

import play.api.libs.json.{Format, JsError, Reads, Writes}

import scala.util.parsing.json.JSONArray

/**
  * Created by adelegue on 01/09/2016.
  */


object FList {

  def apply[A](max: Int, l: List[A]): FList[A] = new FList[A](max, l)

  def empty[A](size: Int) = new FList(size, List.empty[A])

  def reads[A](implicit reads: Reads[A]): Reads[FList[A]] = Reads[FList[A]] {
    case a: JSONArray => Reads.list(reads).reads(a).map { l =>
      new FList(l.size, l)
    }
    case e => JsError("Error reading FList")
  }

  def write[A](implicit write: Writes[A]) = Writes[FList[A]] { flist =>
    Writes.list[A](write).writes(flist.toList)
  }

  implicit def format[A](implicit f: Format[A]) = Format(reads[A](f), write[A](f))
}

class FList[A](val max: Int, val l: List[A]) {

  def add(elt: A): FList[A] = {
    if (l.size == max) {
      new FList(max, elt :: l.reverse.tail.reverse)
    } else {
      new FList(max, elt :: l)
    }
  }

  def toList = l

  def ::(elt: A) = add(elt)

  override def equals(obj: scala.Any): Boolean = {
    obj match {
      case list: FList[_] =>
        list.l.equals(l)
      case _ =>
        false
    }
  }
}
