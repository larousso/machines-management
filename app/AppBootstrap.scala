import actor.{AlertsActor, CorrelationsActor}
import com.softwaremill.macwire._
import controllers.{Application, Assets}
import play.api.ApplicationLoader.Context
import play.api._
import play.api.routing.Router
import services._
import controllers.Mock
import router.Routes

class AppApplicationLoader extends ApplicationLoader {
  def load(context: Context) = {

    // make sure logging is configured
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment)
    }

    (new BuiltInComponentsFromContext(context) with AppComponents).application
  }
}

trait AppComponents extends BuiltInComponents with AppModule {
  lazy val assets: Assets = wire[Assets]
  lazy val prefix: String = "/"
  lazy val router: Router = wire[Routes]
}

trait AppModule extends BuiltInComponents {

  lazy val alertsActor = actorSystem.actorOf(AlertsActor.props(), "alerts")
  lazy val correlationsActor = actorSystem.actorOf(CorrelationsActor.props(), "correlations")


  val apiRoot = configuration.getString("api.root").get //OrElse("http://localhost:9000/api/v1")
  Logger.logger.info("ROOT API {}", apiRoot)

  lazy val machines = Machines(apiRoot)(actorSystem)
  lazy val sensors = Sensors(apiRoot)(actorSystem)

  lazy val machinesMonitor = MachinesMonitor(machines)(actorSystem)
  lazy val machinesUsageCorrelation = MachinesUsageCorrelation(machines, sensors)(actorSystem)

  // Define your dependencies and controllers
  lazy val applicationController = Application(machinesMonitor, machinesUsageCorrelation, alertsActor, correlationsActor, actorSystem)
  lazy val mockController = wire[Mock]

}