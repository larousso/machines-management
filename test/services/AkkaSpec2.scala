package services

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import org.specs2.mutable.After
import org.specs2.specification.Scope

/**
  * Created by adelegue on 23/04/2016.
  */
class AkkaSpec2 extends TestKit(ActorSystem("TestActorSystem")) with After with ImplicitSender with Scope {
  def after = TestKit.shutdownActorSystem(system)
}