package services

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.scaladsl.Sink
import org.specs2.mutable.Specification
import services.Machines.Machine

import scala.collection.immutable
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

/**
  * Created by adelegue on 30/08/2016.
  */
class MachinesTest extends Specification {

  "Machines" should {

    "get all machines" in new AkkaSpec2 with HttpServer {
      override def port = 8084

      override def route: Route =
        path("machines") {
          get {
            complete(HttpEntity(ContentTypes.`application/json`,
              """
                |[
                |  "$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5",
                |  "$API_ROOT/machine/e9c8ae10-a943-49e0-979e-71d125132c64"
                |]
              """.stripMargin))
          }
        }


      private val monitor: Machines = new Machines(s"http://localhost:$port")

      private val expected: Seq[String] = Seq("$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5", "$API_ROOT/machine/e9c8ae10-a943-49e0-979e-71d125132c64")
      Await.result(monitor.getAllMachines, 1 minute) must be equalTo expected

    }


    "get one machine" in new AkkaSpec2 with HttpServer {

      override def port = 8081

      override def route: Route =
        path("machine" / "0e079d74-3fce-42c5-86e9-0a4ecc9a26c5") {
          get {
            complete(HttpEntity(ContentTypes.`application/json`,
              """
                |{
                |  "name": "DMG DMU 40eVo [#50]",
                |  "timestamp": "2016-04-21T07:05:02.495540",
                |  "current": 12.21,
                |  "state": "working",
                |  "location": "0.0,0.0",
                |  "current_alert": 14,
                |  "type": "mill"
                |}
              """.stripMargin))
          }
        }

      private val monitor = new Machines(s"http://localhost:$port")
      val date = LocalDateTime.parse("2016-04-21T07:05:02.495540", DateTimeFormatter.ISO_LOCAL_DATE_TIME)
      Await.result(monitor
        .getMachine("$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"), 1 minute) must beSome(("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5", Machine("DMG DMU 40eVo [#50]", date, 12.21, "working", "0.0,0.0", 14, "mill")))

    }

  }

}
