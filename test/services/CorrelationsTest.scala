package services

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.scaladsl.Sink
import model.{Correlation, Record}
import org.specs2.mutable.Specification
import utils.FList

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by adelegue on 24/04/2016.
  */
class CorrelationsTest extends Specification {

  sequential

  "MachinesUsageCorrelation" should {

    val dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME

    "correlation stream" in new AkkaSpec2 with HttpServer {

      override def port = 8086

      var machineValues: List[String] = List(
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02",
          "current": 11,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 14,
          "type": "mill"
        }
        """,
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02",
          "current": 12,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 14,
          "type": "mill"
        }
        """,
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02",
          "current": 13,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 12.4,
          "type": "mill"
        }
        """,
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02",
          "current": 13,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 12.4,
          "type": "mill"
        }
        """)

      var sensorValues: List[String] = List(
        """
          {
            "pressure":["2015-11-12T17:41:00",1001.97],
            "temperature":["2015-11-12T17:41:00",21.83],
            "humidity":["2015-11-12T17:41:00",86.56]
          }
        """,
        """
          {
            "pressure":["2015-11-12T17:41:00",1001.98],
            "temperature":["2015-11-12T17:41:00",21.84],
            "humidity":["2015-11-12T17:41:00",86.57]
          }
        """,
        """
          {
            "pressure":["2015-11-12T17:41:00",1001.99],
            "temperature":["2015-11-12T17:41:00",21.85],
            "humidity":["2015-11-12T17:41:00",86.58]
          }
        """,
        """
          {
            "pressure":["2015-11-12T17:41:00",1001.99],
            "temperature":["2015-11-12T17:41:00",21.85],
            "humidity":["2015-11-12T17:41:00",86.58]
          }
        """)


      override def route: Route =
        path("machines") {
          get {

            complete(HttpEntity(ContentTypes.`application/json`,
              """
                |[
                |  "$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"
                |]
              """.stripMargin))
          }
        } ~
          path("machine" / "0e079d74-3fce-42c5-86e9-0a4ecc9a26c5") {
            get {
              val head = machineValues.headOption
              if (machineValues.nonEmpty) {
                machineValues = machineValues.tail
                head
                  .map(rep => complete(HttpEntity(ContentTypes.`application/json`, rep)))
                  .getOrElse(complete(StatusCodes.NotFound -> ""))
              } else {
                complete(StatusCodes.NotFound -> "")
              }
            }
          } ~
          path("env-sensor") {
            get {
              val head = sensorValues.headOption
              if (machineValues.nonEmpty) {
                sensorValues = sensorValues.tail
                head
                  .map(rep => complete(HttpEntity(ContentTypes.`application/json`, rep)))
                  .getOrElse(complete(StatusCodes.NotFound -> ""))
              } else {
                complete(StatusCodes.NotFound -> "")
              }
            }
          }

      private val monitor: MachinesUsageCorrelation = new MachinesUsageCorrelation(Machines(s"http://localhost:$port"), Sensors(s"http://localhost:$port"))
      val date = LocalDateTime.parse("2016-04-21T07:05:02.495540", DateTimeFormatter.ISO_LOCAL_DATE_TIME)
      val correlations = monitor.correlationStream().take(3).runWith(Sink.seq)

      val expected = List(
        Correlation(Some("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"),
          values = FList(1, List(
            Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 11))),
          temperatures = FList(1, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.83))),
          pressures = FList(1, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.97))),
          humidity = FList(1, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.56)))
        ),
        Correlation(Some("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"),
          values = FList(2, List(
            Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 12),
            Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 11))),
          temperatures = FList(2, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.84),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.83))),
          pressures = FList(2, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.98),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.97))),
          humidity = FList(2, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.57),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.56)))
        ),
        Correlation(Some("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"),
          values = FList(3, List(
            Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 13),
            Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 12),
            Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 11))),
          temperatures = FList(2, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.85),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.84),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.83))),
          pressures = FList(2, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.99),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.98),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.97))),
          humidity = FList(2, List(
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.58),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.57),
            Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.56)))
        ))


      val first :: second :: third :: tail = Await.result(correlations, 1 minute).toList

      first must be equalTo Correlation(Some("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"),
        values = FList(1, List(
          Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 11))),
        temperatures = FList(1, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.83))),
        pressures = FList(1, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.97))),
        humidity = FList(1, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.56)))
      )

      second must be equalTo Correlation(Some("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"),
        values = FList(2, List(
          Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 12),
          Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 11))),
        temperatures = FList(2, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.84),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.83))),
        pressures = FList(2, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.98),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.97))),
        humidity = FList(2, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.57),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.56)))
      )

      third must be equalTo Correlation(Some("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"),
        values = FList(3, List(
          Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 13),
          Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 12),
          Record(LocalDateTime.of(2016, 4, 21, 7, 5, 2, 0), 11))),
        temperatures = FList(2, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.85),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.84),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 21.83))),
        pressures = FList(2, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.99),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.98),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 1001.97))),
        humidity = FList(2, List(
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.58),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.57),
          Record(LocalDateTime.of(2015, 11, 12, 17, 41, 0), 86.56)))
      )

    }
  }

}
