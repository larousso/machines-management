package services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server._
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import org.specs2.mutable.{After, Before}

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by adelegue on 24/04/2016.
  */
trait HttpServer extends After with Before {

  def route: Route

  def port = 8080

  implicit def system: ActorSystem

  implicit def materializer = ActorMaterializer()(system)

  implicit val executionContext = system.dispatcher

  var server: Option[ServerBinding] = None

  override def before = {
    server = Some(Await.result(Http(system).bindAndHandle(route, "localhost", port), 1 second))
  }

  override def after = {
    server.foreach(s => Await.result(s.unbind(), 1 second))
    TestKit.shutdownActorSystem(system)
  }
}
