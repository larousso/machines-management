package services

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.scaladsl.Sink
import org.specs2.mutable.Specification
import model.Alert

import scala.collection.immutable
import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by adelegue on 24/04/2016.
  */
class MachinesMonitorTest extends Specification {

  sequential

  "MachinesMonitor" should {


    "alerts stream" in new AkkaSpec2 with HttpServer {

      override def port = 8083

      var reps: List[String] = List(
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02.495540",
          "current": 11,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 14,
          "type": "mill"
        }
        """,
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02.495540",
          "current": 12,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 14,
          "type": "mill"
        }
        """,
        """
        {
          "name": "DMG DMU 40eVo [#50]",
          "timestamp": "2016-04-21T07:05:02.495540",
          "current": 13,
          "state": "working",
          "location": "0.0,0.0",
          "current_alert": 12.4,
          "type": "mill"
        }
        """)


      override def route: Route =
        path("machines") {
          get {

            complete(HttpEntity(ContentTypes.`application/json`,
              """
                |[
                |  "$API_ROOT/machine/0e079d74-3fce-42c5-86e9-0a4ecc9a26c5"
                |]
              """.stripMargin))
          }
        } ~
          path("machine" / "0e079d74-3fce-42c5-86e9-0a4ecc9a26c5") {
            get {
              val head = reps.headOption
              reps = reps.tail
              head
                .map(rep => complete(HttpEntity(ContentTypes.`application/json`, rep)))
                .getOrElse(complete(HttpEntity(ContentTypes.`application/json`, "")))

            }
          }

      private val monitor: MachinesMonitor = new MachinesMonitor(Machines(s"http://localhost:$port"))
      val date = LocalDateTime.parse("2016-04-21T07:05:02.495540", DateTimeFormatter.ISO_LOCAL_DATE_TIME)
      val machines = monitor.alertsStream().take(1).runWith(Sink.seq)

      val mesures = Seq(11.0, 12.0, 13.0)
      val alert = Alert("0e079d74-3fce-42c5-86e9-0a4ecc9a26c5", mesures, mesures.sum / mesures.size, 13, 12.4)
      Await.result(machines, 1 minute) must be equalTo immutable.Seq(alert)

    }
  }

}
