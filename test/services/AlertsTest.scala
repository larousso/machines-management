package services

import actor.AlertsActor
import akka.actor.ActorRef
import akka.testkit.{TestActorRef, TestProbe}
import org.specs2.mutable.Specification
import actor.AlertsActor.Listener

import model.Alert

/**
  * Created by adelegue on 23/04/2016.
  */
class AlertsTest extends Specification {

  "Alerts" should {
    "add listener should fill listeners list" in new AkkaSpec2 {

      val alerts = TestActorRef[AlertsActor](AlertsActor.props())

      val ref: ActorRef = TestProbe().ref
      alerts ! Listener(ref)

      alerts.underlyingActor.toNotify must be equalTo List(ref)
    }

    "new alert should add alert to history and notify listeners" in new AkkaSpec2 {
      val alerts = TestActorRef[AlertsActor](AlertsActor.props())

      val listener = TestProbe()
      alerts ! Listener(listener.ref)
      val alert = Alert("id", Seq(1, 2, 3), 2, 2, 1)
      alerts ! alert

      alerts.underlyingActor.lastAlerts must be equalTo List(alert)
      listener expectMsg alert
    }
  }

}
