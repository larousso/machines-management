package services

import java.time.LocalDateTime

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import org.specs2.mutable.Specification
import services.Sensors.{Sensor, Value}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

/**
  * Created by adelegue on 30/08/2016.
  */
class SensorsTest extends Specification {

  "Sensors" should {

    "get all machines" in new AkkaSpec2 with HttpServer {
      override def port = 8085

      override def route: Route =
        path("env-sensor") {
          get {
            complete(HttpEntity(ContentTypes.`application/json`,
              """
                {
                |"pressure":["2015-11-12T17:41:00",1001.97],
                |"temperature":["2015-11-12T17:41:00",21.83],
                |"humidity":["2015-11-12T17:41:00",86.56]
                |}
              """.stripMargin))
          }
        }


      private val monitor: Sensors = new Sensors(s"http://localhost:$port")

      private val expected = Sensor(
        pressure = Value(LocalDateTime.of(2015, 11, 12, 17, 41), 1001.97),
        temperature = Value(LocalDateTime.of(2015, 11, 12, 17, 41), 21.83),
        humidity = Value(LocalDateTime.of(2015, 11, 12, 17, 41), 86.56)
      )
      Await.result(monitor.getSensor, 1 minute) must beSome(expected)

    }
  }
}
